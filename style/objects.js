// console.log("hello");
// Data Types:
// 1. String
// 2. Number
// 3. Boolean
// 4. Undefined --> variables that are declared but
// without assigned values.
// 5. Null --> no value, if the operation cannot find a value
//6. Objects --> collection of related values.
// denoted by {}. Presented in key-value pairs.

// key or property key/ property name
const student = {
	name: "Brandon",
	studentNumber: "2014-15233",
	course: "BS Physics",
	college: "Science",
	department: "Physics",
	age: 22,
	isSingle: true,
	motto: "Time is gold",
	address: {
		stName: "Mahogany St.",
		brgy: "Poblacion",
		city: "Makati City",
		zipCode: "12345"
// Multidiventional object
	},
// Inside object, no property name. Anonymous function
//A function inside an object is also called "Method".
	showAge: function (){
		console.log(student.age);
		return student.age;
	},
	addAge: function (){
		// add 1 to the age property of student.
		student.age += 1;
		return "Successfully added age!"
	}

};

const student2 ={
	name: "Brenda"
}

// call student.showAge ();
// NOTE: Values of properties does now have access to each one
//To access a specific key/ property of an object, we will use the dot 
// notation
//If you us dot notation after an object that does not exist
// it will cause an error

// If we access a property that does not exist it will return undefined.

// to add a property in an object, we will use dot notation and aside the value

student.gender = "Male";

// to update the value of the property we will use dot notation and assign a new value

student.isSingle = false;

// to delete a property, we will use the keyword delete then object.propertyName

delete student.studentNumber;

