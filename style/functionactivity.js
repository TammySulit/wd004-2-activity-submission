// Answer 1:
  function getSum(num1, num2){
	const sum = num1 + num2;
	return sum;
}

// Answer 2:
	function getDifference(num1, num2){
	const difference = num1 - num2;
	return difference;
}

// Answer 3:

  function getProduct(num1, num2){
  const product = num1 * num2;
  return product;
}


// Answer 4:
  function getQuotient(num1, num2){
  const quotient = num1 / num2;
  return quotient;
}


// Alert Section!
// The client asked to create the following functions too!
// And the client specifically asked:
//   "PLEASE DO NOT USE TWO ARITHMETIC OPERATORS IN ONE LINE"

// 1. getCylinderVolume(), pi is a global constant, this function accepts 2 parameters, radius and height.
// 2. getTrapezoidArea(), this function accepts 3 parameters, base1, base2, and height.

// Answer 1:
function getCylinderVolume(radius, height){
  const radius2 = radius * radius;
  const area = radius2 * 3.1416;
  const volume =  height * area;
  return volume;
}

// Answer 2:
function getTrapezoidArea(base1, base2, height){
const tr = base1 + base2;
const ht = height / 2;
const area = tr * ht;
return area;
}


// STRETCH GOAL:
// 1. getTotalAmount(), this function accepts 4 parameters, the cost of the item, the quantity of the item, 
// the amount of discount in whole number (ex: if the discount is 20%, the argument will be 20), and the amount 
// of tax in whole number (ex: if the tax amount is 15%, the argument will be 15).
//   Remember:
//   Discount will be applied first before adding the tax.
  
function getTotalAmount(cost, quantity, discount,tax){
  //converted the discount rate into the decimal
  const covnertedDiscount = discount / 100;
  //converted tax into decimal
  const convertedTax = tax/ 100;
  // got the total amount without Discount
  const amountWithoutDiscount = cost * quantity;
  // computed discount
  const discountRate = amountWithoutDiscount * convertedDiscount;
  //deduct discount
  const discountedAmounted = amountWithoutDiscount - discountRate;
  //compute tax
  const taxRate = discountedAmount * convertedTax;
  //finally, add the tax to get the total amount
  const totalAmount = discountedAmount + taxRate

  return totalAmount;
}


STRESS GOAL:
"This will not merit additional points, but you will have my admiration."
"You are free to shorten the code as you deem necessary"

1. getTotalSalary(), this function accepts 5 parameters, the monthly salary of the employee, the total working
days in a month, number of days the employee was absent, the number of minutes the employee was late, and the
amount of tax in whole number.
  Remember:
  Deduct absences and late first before adding the tax. 