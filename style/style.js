console.log("hello");


const num1 = 2;
const num2 = 4;

const sum1 = num1 + num2;
console.log(sum1);

const num3 = 5;
const num4 = 3;

const sum2 = num3 + num4;
console.log(sum2);

const num5 = 11;
const num6 = 3;

const sum3 = num5 + num6;
console.log(sum3);

//const num11

//DRY Principle
// Do not repeat yourself

// Functions:
// subprogram that is designed to do a particular task
// We need a function that will add 2 numbers
// syntax:
// Function:nameOfTheFunction(){
// 	// task or what your function should be	
// }

// remember, just like your variables function names should be descriptive
// parenthesis: parameters - values that the function need in order to do its tasks
// CamelCase - first word lower case, next word upper case
//If th function does not have a return value it will still return 
// an undefined data


function getSum(num1, num2){
	const sum = num1 + num2;
	console.log(sum);
	return sum;
}


// function call 
// Arguments - these are the values that we will send to the function
// getSum(2,3); //2 is num1, 3 is num 2
// getSum(4,5);
// getSum(9,8);

const ageSum = getSum(15, 18);
console.log(ageSum);

